package magicgoose.common.collections;

public class ReadonlyArrayIntList extends BaseIntList {

    private final int[] a;

    public ReadonlyArrayIntList(int[] array) {
        this.a = array;
    }

    @Override
    public int length() {
        return a.length;
    }

    @Override
    public int get(int index) {
        return a[index];
    }
}
