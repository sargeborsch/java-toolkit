package magicgoose.common.collections;

interface MutableIntList extends IntList {
    void set(int index, int value);
}
