package magicgoose.common.collections;

public interface IntList {
    int length();

    int get(int index);
}
