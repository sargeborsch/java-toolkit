package magicgoose.common.collections;

import magicgoose.common.function.BooleanFunction1;
import magicgoose.common.function.ObjectDoubleFunction;
import magicgoose.common.function.ObjectLongFunction;
import magicgoose.common.util.IterableTools;
import rx.functions.Func1;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class IterableOps<T> implements Iterable<T> {

    public final Iterable<T> result;

    private IterableOps(Iterable<T> result) {
        this.result = result;
    }

    public static <T> IterableOps<T> from(Iterable<T> src) {
        return new IterableOps<>(src);
    }

    @Override
    public Iterator<T> iterator() {
        return result.iterator();
    }

    public <B> IterableOps<B> map(Func1<T, B> fun) {
        return IterableOps.from(IterableTools.map(this, fun));
    }

    public <B> IterableOps<B> flatMap(Func1<T, Iterable<B>> fun) {
        return IterableOps.from(IterableTools.flatMap(this, fun));
    }

    public IterableOps<T> filter(BooleanFunction1<T> fun) {
        return IterableOps.from(IterableTools.filter(this, fun));
    }

    public <B> IterableOps<B> filterByType(Class<B> c) {
        return IterableOps.from(IterableTools.filterByType(this, c));
    }

    public List<T> toList() {
        return IterableTools.toList(this);
    }

    public <K extends Comparable<K>> List<T> sortBy(Func1<T, K> keyFun, boolean reverse) {
        return IterableTools.sortBy(this, keyFun, reverse);
    }

    public <K extends Comparable<K>> List<T> sortBy(Func1<T, K> keyFun) {
        return IterableTools.sortBy(this, keyFun);
    }

    public T first() {
        return IterableTools.first(this);
    }

    public IterableOps<T> notNull() {
        return IterableOps.from(IterableTools.notNull(this));
    }

    public T[] toArray(final Class<T> type) {
        return IterableTools.toArray(this, type);
    }

    public <K, V> Map<K, V> toMap(Func1<T, K> keyFun, Func1<T, V> valueFun) {
        return IterableTools.toMap(this, keyFun, valueFun);
    }

    public T min(ObjectDoubleFunction<T> fun) {
        return IterableTools.min(this, fun);
    }

    public T min(ObjectLongFunction<T> fun) {
        return IterableTools.min(this, fun);
    }

    public <N extends Comparable<N>> T min(Func1<T, N> fun) {
        return IterableTools.min(this, fun);
    }

    public T max(ObjectDoubleFunction<T> fun) {
        return IterableTools.max(this, fun);
    }

    public T max(ObjectLongFunction<T> fun) {
        return IterableTools.max(this, fun);
    }

    public <N extends Comparable<N>> T max(Func1<T, N> fun) {
        return IterableTools.max(this, fun);
    }
}
