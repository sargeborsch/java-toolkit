package magicgoose.common;

public class Debug {
    public static void assertEquals(boolean a, boolean b) {
        if (a != b)
            throw new AssertionError();
    }

    public static void assertEquals(int a, int b) {
        if (a != b)
            throw new AssertionError();
    }

    public static void assertTrue(boolean b) {
        if (!b)
            throw new AssertionError();
    }

    public static void assertNotNull(Object o) {
        if (o == null)
            throw new AssertionError();
    }

    public static void assertEquals(long a, long b) {
        if (a != b)
            throw new AssertionError();
    }
}
