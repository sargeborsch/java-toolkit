package magicgoose.common.function;

public interface CharBooleanFunction {
    boolean call(char x);
}
