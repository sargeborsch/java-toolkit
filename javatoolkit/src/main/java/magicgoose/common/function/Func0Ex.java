package magicgoose.common.function;

import rx.functions.Function;

import java.util.concurrent.Callable;


public interface Func0Ex<R> extends Function, Callable<R> {
    @Override
    R call() throws Exception;
}