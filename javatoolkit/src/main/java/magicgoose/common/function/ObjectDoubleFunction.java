package magicgoose.common.function;

public interface ObjectDoubleFunction<T> {
    double call(T arg);
}
