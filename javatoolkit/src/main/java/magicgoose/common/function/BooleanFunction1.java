package magicgoose.common.function;

public interface BooleanFunction1<A> {
    boolean call(A arg);
}
