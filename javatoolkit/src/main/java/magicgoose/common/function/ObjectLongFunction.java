package magicgoose.common.function;

public interface ObjectLongFunction<T> {
    long call(T arg);
}
