package magicgoose.common.function;

public interface BooleanAction {
    void run(boolean x);
}
