package magicgoose.common.function;

public interface Func1Ex<A, B> {
    B call(A arg) throws Exception;
}
