package magicgoose.common;

public class ArrayUtil {
    public static void overwrite(final int[][] target, final int[][] source) {
        Debug.assertEquals(target.length, source.length);

        for (int i = 0; i < source.length; i++) {
            final int[] sourceRow = source[i];
            final int[] targetRow = target[i];
            Debug.assertEquals(sourceRow.length, targetRow.length);

            System.arraycopy(sourceRow, 0, targetRow, 0, sourceRow.length);
        }
    }
}
