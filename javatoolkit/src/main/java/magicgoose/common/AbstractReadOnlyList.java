package magicgoose.common;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AbstractReadOnlyList<T> implements List<T> {

    private final int size;
    private final GetFun<T> getFun;

    public AbstractReadOnlyList(int size, GetFun<T> getFun) {
        this.getFun = getFun;
        this.size = size;
    }

    @Override
    public T get(int i) {
        return getFun.get(i);
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void add(int i, T view) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T view) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> views) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends T> views) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size(); ++i)
            if (get(i).equals(o))
                return true;
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        for (Object object : objects) {
            if (!this.contains(object))
                return false;
        }
        return true;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size(); i++) {
            if (get(i).equals(o))
                return i;
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            int i = 0;

            @Override
            public boolean hasNext() {
                return i < size();
            }

            @Override
            public T next() {
                return get(i++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = this.size() - 1; i >= 0; i--) {
            if (get(i).equals(o))
                return i;
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T set(int i, T view) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> subList(int i, int i2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public <TA> TA[] toArray(TA[] array) {
        throw new UnsupportedOperationException();
    }

    public static interface GetFun<T> {
        T get(int i);
    }

}
