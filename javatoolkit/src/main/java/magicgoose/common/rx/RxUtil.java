package magicgoose.common.rx;

import magicgoose.common.Either;
import rx.Observable;

public class RxUtil {
    public static <L, R> Observable<L> left(Observable<Either<L, R>> input) {
        return input.filter(x -> !x.isRight).map(x -> x.left);
    }

    public static <L, R> Observable<R> right(Observable<Either<L, R>> input) {
        return input.filter(x -> x.isRight).map(x -> x.right);
    }
}
