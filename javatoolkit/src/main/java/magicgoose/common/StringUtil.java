package magicgoose.common;

import magicgoose.common.function.CharBooleanFunction;

public class StringUtil {
    public static String multiply(String s, int count) {
        final StringBuilder builder = new StringBuilder(s.length() * count);
        for (int i = 0; i < count; i++) {
            builder.append(s);
        }
        return builder.toString();
    }

    public static String join(Iterable<String> items, String sep) {
        final StringBuilder sb = new StringBuilder();
        boolean insertSep = false;
        for (final String item : items) {
            if (insertSep)
                sb.append(sep);
            insertSep = true;
            sb.append(item);
        }
        return sb.toString();
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isNullOrWhitespace(String s) {
        return s == null || isWhitespace(s);
    }

    private static boolean isWhitespace(String s) {
        int length = s.length();

        for (int i = 0; i < length; i++) {
            if (!Character.isSpaceChar(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String trim(String src) {
        return trim(src, Character::isSpaceChar);
    }

    public static String trim(final String src, final CharBooleanFunction p) {
        int start = 0;
        int end = src.length();

        while (end > 0 && p.call(src.charAt(end - 1)))
            --end;

        if (end == 0)
            return "";

        while (p.call(src.charAt(start)))
            ++start;

        return src.substring(start, end);
    }
}
