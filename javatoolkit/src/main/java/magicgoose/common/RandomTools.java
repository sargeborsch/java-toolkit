package magicgoose.common;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;

import java.util.Random;

public class RandomTools {

    private static int intInRange(Random generator, int min, int max) {
        return min + generator.nextInt(max - min);
    }

    public static void shuffle(Random generator, int[] array, int chunkLength) {
        for (int i = 0; i < chunkLength - 1; i++) {
            final int itemToSwap = intInRange(generator, i, chunkLength);
            if (i != itemToSwap) {
                swap(array, i, itemToSwap);
            }
        }
    }

    private static void swap(int[] array, int a, int b) {
        final int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

    public static int intInRangeExcept(Random generator, int min, int max, TIntSet exclusions) {

        final TIntArrayList allowedItems = new TIntArrayList();

        for (int x = min; x < max; x++) {
            if (!exclusions.contains(x))
                allowedItems.add(x);
        }

        return pickRandomItem(generator, allowedItems);
    }

    private static int pickRandomItem(Random generator, TIntList allowedItems) {
        final int index = generator.nextInt(allowedItems.size());
        return allowedItems.get(index);
    }
}
