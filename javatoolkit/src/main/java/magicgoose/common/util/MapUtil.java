package magicgoose.common.util;

import rx.functions.Func1;

import java.util.Map;

public class MapUtil {
    public static <K, V> V getOrUpdate(Map<K, V> map, K key, Func1<K, V> valueFun) {
        if (map.containsKey(key)) {
            return map.get(key);
        }

        final V value = valueFun.call(key);
        map.put(key, value);
        return value;
    }
}
