package magicgoose.common.util;

public class ComparableUtil {
    public static <N extends Comparable<N>> boolean lt(final N a, final N b) {
        return a.compareTo(b) < 0;
    }

    public static <N extends Comparable<N>> boolean gt(final N a, final N b) {
        return a.compareTo(b) > 0;
    }

    public static <N extends Comparable<N>> boolean lessThanOrEqual(final N a, final N b) {
        return a.compareTo(b) <= 0;
    }

    public static <N extends Comparable<N>> boolean greaterThanOrEqual(final N a, final N b) {
        return a.compareTo(b) >= 0;
    }
}
