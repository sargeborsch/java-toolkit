package magicgoose.common.util;

public class Objects {
    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    @SafeVarargs
    public static <T> T firstNotNull(T... objects) {
        for (final T object : objects) {
            if (object != null)
                return object;
        }
        return null;
    }
}
