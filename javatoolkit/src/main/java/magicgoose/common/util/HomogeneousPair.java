package magicgoose.common.util;

public final class HomogeneousPair<T> {
    public final T first;
    public final T second;


    private HomogeneousPair(final T first, final T second) {
        this.first = first;
        this.second = second;
    }

    public static <T> HomogeneousPair<T> create(T a, T b) {
        return new HomogeneousPair<>(a, b);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final HomogeneousPair<?> that = (HomogeneousPair<?>) o;

        return first.equals(that.first) && second.equals(that.second);
    }

    @Override
    public int hashCode() {
        int result = first.hashCode();
        result = 31 * result + second.hashCode();
        return result;
    }
}
