package magicgoose.common.util;

import java.util.Iterator;

public abstract class ReadonlyIteratorBase<T> implements Iterator<T> {
    @Override
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
