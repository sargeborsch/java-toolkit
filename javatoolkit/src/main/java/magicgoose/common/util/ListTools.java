package magicgoose.common.util;

import magicgoose.common.AbstractReadOnlyList;
import rx.functions.Func1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListTools {

    private final static List<Object> emptyList = new AbstractReadOnlyList<>(0, null);

    public static <A> List<A> emptyList() {
        //noinspection unchecked
        return (List<A>) emptyList;
    }

    /**
     * Not lazy, copies data
     */
    public static <A, B> List<B> map(List<A> input, Func1<A, B> fun) {
        final ArrayList<B> result = new ArrayList<B>(input.size());
        for (final A elem : input) {
            result.add(fun.call(elem));
        }
        return result;
    }

    /**
     * Doesn't minimize key computation count — beware
     */
    public static <A, K extends Comparable<K>> void sortByInPlace(List<A> input, Func1<A, K> keyFun, boolean reverse) {

        final int multiplier = reverse ? -1 : 1;

        Collections.sort(input, (a, b) -> {
            final K aKey = keyFun.call(a);
            final K bKey = keyFun.call(b);
            return aKey.compareTo(bKey) * multiplier;
        });
    }

    /**
     * Doesn't minimize key computation count — beware
     */
    public static <A, K extends Comparable<K>> void sortByInPlace(List<A> input, Func1<A, K> keyFun) {
        sortByInPlace(input, keyFun, false);
    }

    public static <T> T[] toArray(List<T> items, Class<T> type) {
        final int size = items.size();

        //noinspection unchecked
        T[] result = (T[]) Array.newInstance(type, size);
        result = items.toArray(result);

        return result;
    }
}
