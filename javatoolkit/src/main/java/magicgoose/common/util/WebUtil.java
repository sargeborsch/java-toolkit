package magicgoose.common.util;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class WebUtil {
    public static String downloadContentHTTPS(String path) throws IOException {
        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://" + path).openConnection();
        connection.setRequestMethod("GET");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.connect();

        try (final InputStream inputStream = connection.getInputStream()) {
            return convertStreamToString(inputStream);
        } finally {
            connection.disconnect();
        }
    }

    private static String convertStreamToString(final InputStream inputStream) {
        final Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        if (scanner.hasNext())
            return scanner.next();
        else
            return "";
    }
}
