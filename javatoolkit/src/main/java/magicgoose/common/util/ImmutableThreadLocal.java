package magicgoose.common.util;

import rx.functions.Func0;

public final class ImmutableThreadLocal<T> extends ThreadLocal<T> {

    private final Func0<T> valueFun;

    public ImmutableThreadLocal(final Func0<T> valueFun) {
        this.valueFun = valueFun;
    }

    @Override
    final protected T initialValue() {
        return valueFun.call();
    }

    @Override
    final public void set(final T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    final public void remove() {
        throw new UnsupportedOperationException();
    }
}
