package magicgoose.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionUtil {
    public static <T> Iterable<Field> getDeclaredAndSubclassFields(
            final Class<? extends T> thisClass, final Class<T> baseClass) {

        final ArrayList<List<Field>> fieldLists = new ArrayList<>();

        Class<?> c = thisClass;
        do {
            fieldLists.add(Arrays.asList(c.getDeclaredFields()));

            if (c.equals(baseClass))
                break;

            c = c.getSuperclass();
        } while (true);

        return IterableTools.flatMap(fieldLists, x -> x);
    }
}
