package magicgoose.common.util;

import magicgoose.common.function.BooleanFunction1;
import magicgoose.common.function.ObjectDoubleFunction;
import magicgoose.common.function.ObjectLongFunction;
import rx.functions.Func1;

import java.util.*;

public class IterableTools {

    private static Iterator<Object> emptyIterator = new Iterator<Object>() {
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    };

    public static <A, B> Iterable<B> map(Iterable<A> input, Func1<A, B> fun) {
        return () -> new Iterator<B>() {

            final Iterator<A> src = input.iterator();

            @Override
            public boolean hasNext() {
                return src.hasNext();
            }

            @Override
            public B next() {
                return fun.call(src.next());
            }

            @Override
            public void remove() {
                src.remove();
            }
        };
    }

    public static <A> Iterator<A> emptyIterator() {
        //noinspection unchecked
        return (Iterator<A>) emptyIterator;
    }

    public static <A, B> Iterable<B> flatMap(Iterable<A> input, Func1<A, Iterable<B>> fun) {
        return () -> new ReadonlyIteratorBase<B>() {
            private final Iterator<A> self = input.iterator();
            private Iterator<B> cur = emptyIterator();

            @Override
            public boolean hasNext() {
                return cur.hasNext() ||
                        self.hasNext() && getNext();
            }

            private boolean getNext() {
                cur = fun.call(self.next()).iterator();
                return hasNext(); // TODO — get rid of recursion
            }

            @Override
            public B next() {
                if (hasNext())
                    return cur.next();
                throw new NoSuchElementException();
            }
        };
    }

    public static <A> Iterable<A> filter(Iterable<A> input, BooleanFunction1<A> fun) {
        return () -> new ReadonlyIteratorBase<A>() {

            private final Iterator<A> self = input.iterator();

            private A hd = null;
            private boolean hdDefined = false;

            @Override
            public boolean hasNext() {
                return hdDefined || reevaluateHasNext();
            }

            private boolean reevaluateHasNext() {
                do {
                    if (!self.hasNext()) return false;
                    hd = self.next();
                } while (!fun.call(hd));
                hdDefined = true;
                return true;
            }

            @Override
            public A next() {
                if (hasNext()) {
                    hdDefined = false;
                    return hd;
                }
                throw new NoSuchElementException();
            }
        };
    }

    public static <A, B> Iterable<B> filterByType(Iterable<A> input, Class<B> c) {
        //noinspection unchecked
        return (Iterable<B>) filter(input, c::isInstance);
    }

    @SafeVarargs
    public static <A> Iterable<A> concat(Iterable<A>... inputs) {
        return flatMap(Arrays.asList(inputs), x -> x);
    }

    public static <A> List<A> toList(Iterable<A> input) {
        final ArrayList<A> result = new ArrayList<>();
        for (final A elem : input) {
            result.add(elem);
        }
        return result;
    }

    public static <A, K extends Comparable<K>> List<A> sortBy(Iterable<A> input, Func1<A, K> keyFun, boolean reverse) {
        final List<A> list = toList(input);
        ListTools.sortByInPlace(list, keyFun, reverse);
        return list;
    }

    public static <A, K extends Comparable<K>> List<A> sortBy(Iterable<A> input, Func1<A, K> keyFun) {
        return sortBy(input, keyFun, false);
    }

    public static <A> A first(final Iterable<A> items) {
        return items.iterator().next();
    }

    public static <T> Iterable<T> notNull(final Iterable<T> items) {
        return IterableTools.filter(items, x -> x != null);
    }

    public static <T> T[] toArray(Iterable<T> items, final Class<T> type) {
        return ListTools.toArray(IterableTools.toList(items), type);
    }

    public static <S, K, V> Map<K, V> toMap(final Iterable<S> items, Func1<S, K> keyFun, Func1<S, V> valueFun) {

        final Map<K, V> result = new LinkedHashMap<>();

        for (final S item : items) {
            final K key = keyFun.call(item);
            final V value = valueFun.call(item);
            result.put(key, value);
        }

        return result;
    }

    public static <T> T min(final Iterable<T> items, ObjectDoubleFunction<T> fun) {
        final Iterator<T> iterator = items.iterator();

        T minValue = iterator.next();
        double minRating = fun.call(minValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final double nextRating = fun.call(next);
            if (nextRating < minRating) {
                minValue = next;
                minRating = nextRating;
            }
        }

        return minValue;
    }

    public static <T> T min(final Iterable<T> items, ObjectLongFunction<T> fun) {
        final Iterator<T> iterator = items.iterator();

        T minValue = iterator.next();
        long minRating = fun.call(minValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final long nextRating = fun.call(next);
            if (nextRating < minRating) {
                minValue = next;
                minRating = nextRating;
            }
        }

        return minValue;
    }

    public static <T, N extends Comparable<N>> T min(final Iterable<T> items, Func1<T, N> fun) {
        final Iterator<T> iterator = items.iterator();

        T minValue = iterator.next();
        N minRating = fun.call(minValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final N nextRating = fun.call(next);
            if (ComparableUtil.lt(nextRating, minRating)) {
                minValue = next;
                minRating = nextRating;
            }
        }

        return minValue;
    }
    public static <T> T max(final Iterable<T> items, ObjectDoubleFunction<T> fun) {
        final Iterator<T> iterator = items.iterator();

        T maxValue = iterator.next();
        double maxRating = fun.call(maxValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final double nextRating = fun.call(next);
            if (nextRating > maxRating) {
                maxValue = next;
                maxRating = nextRating;
            }
        }

        return maxValue;
    }

    public static <T> T max(final Iterable<T> items, ObjectLongFunction<T> fun) {
        final Iterator<T> iterator = items.iterator();

        T maxValue = iterator.next();
        long maxRating = fun.call(maxValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final long nextRating = fun.call(next);
            if (nextRating > maxRating) {
                maxValue = next;
                maxRating = nextRating;
            }
        }

        return maxValue;
    }

    public static <T, N extends Comparable<N>> T max(final Iterable<T> items, Func1<T, N> fun) {
        final Iterator<T> iterator = items.iterator();

        T maxValue = iterator.next();
        N maxRating = fun.call(maxValue);

        while (iterator.hasNext()) {
            final T next = iterator.next();

            final N nextRating = fun.call(next);
            if (ComparableUtil.gt(nextRating, maxRating)) {
                maxValue = next;
                maxRating = nextRating;
            }
        }

        return maxValue;
    }
}
