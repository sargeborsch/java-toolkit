package magicgoose.common.util;

import java.util.ArrayList;

public class ActionQueue {
    private final ArrayList<Runnable> actionsList = new ArrayList<>();
    private boolean isDeferring = true;

    public void setDeferring(boolean isDeferring) {
        synchronized (actionsList) {
            if (this.isDeferring == isDeferring)
                return;

            this.isDeferring = isDeferring;
            if (!isDeferring) {
                runAllAndClear();
            }
        }
    }

    private void runAllAndClear() {
        for (final Runnable runnable : actionsList) {
            runnable.run();
        }
        actionsList.clear();
    }

    public void add(Runnable action) {
        synchronized (actionsList) {
            if (this.isDeferring) {
                actionsList.add(action);
            } else {
                action.run();
            }
        }
    }
}
