package magicgoose.common;

public class Either<L, R> {

    public final boolean isRight;
    public final L left;
    public final R right;

    private Either(boolean isRight, L left, R right) {
        this.isRight = isRight;
        this.left = left;
        this.right = right;
    }

    public static <L, R> Either<L, R> createLeft(L left) {
        return new Either<>(false, left, null);
    }

    public static <L, R> Either<L, R> createRight(R right) {
        return new Either<>(true, null, right);
    }
}
