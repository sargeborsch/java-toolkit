package magicgoose.common;

public class MathUtil {
    public static int positiveRemainder(int a, int d) {
        return ((a % d) + d) % d;
    }
}
